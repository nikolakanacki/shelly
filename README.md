# SHELLY

A set of bash helpers developed and maintained by the oh:lab team.

## Install

### Assets

a) Clone this git repo anywhere on your hard drive and symlink it to `~/.shelly`:
```
$ git clone git clone https://nikolakanacki@bitbucket.org/nikolakanacki/shelly.git /path/to/repos/shelly
$ ln -s /path/to/repos/shelly ~/.shelly
```
b) Or clone it directly to `~/.shelly`:
```
$ git clone git clone https://nikolakanacki@bitbucket.org/nikolakanacki/shelly.git ~/.shelly
```

### Bash

Add the code below at the end of your `~/.profile` or `~/.bash_profile` file:

```
# SHELLY
if [ -d "$HOME/.shelly" ] ; then
  PATH="$HOME/.shelly/bin:$PATH"
  source $HOME/.shelly/bashrc
fi
```

### Nano

Add the code below at the end of your `~/.nanorc` file (create it if you don't have one):

```
# SHELLY

# Tabs to spaces and it's size to 2
set tabsize 2
set tabstospaces

# Matching brackets
set matchbrackets "(<[{)>]}"

# Color modes
include "~/.shelly/nanomodes/apacheconf.nanorc"
include "~/.shelly/nanomodes/conf.nanorc"
include "~/.shelly/nanomodes/css.nanorc"
include "~/.shelly/nanomodes/git.nanorc"
include "~/.shelly/nanomodes/gitcommit.nanorc"
include "~/.shelly/nanomodes/html.nanorc"
include "~/.shelly/nanomodes/js.nanorc"
include "~/.shelly/nanomodes/json.nanorc"
include "~/.shelly/nanomodes/man.nanorc"
include "~/.shelly/nanomodes/markdown.nanorc"
include "~/.shelly/nanomodes/php.nanorc"
include "~/.shelly/nanomodes/yaml.nanorc"

```
