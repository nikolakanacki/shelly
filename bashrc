# Make os variable hold MAC/LINUX, respectively

if [[ "$OSTYPE" == "linux-gnu" ]] ; then
  os="LINUX";
elif [[ "$OSTYPE" == "darwin"* ]] ; then
  os="MAC";
fi

# Linux LS commands, with colors

if [ $os = "MAC" ] ; then
  alias ls='ls -G'
  alias ll='ls -alF'
  alias la='ls -A'
  alias l='ls -CF'
fi

#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
  if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    color_prompt=yes
  else
    color_prompt=
  fi
fi

if [ "$color_prompt" = yes ]; then
  PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\H\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '
else
  PS1='${debian_chroot:+($debian_chroot)}\H:\W\$ '
fi

unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\H: \W\a\]$PS1"
    ;;
*)
    ;;
esac
